﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProStream
{
    public interface IVehicle
    {
        string Location { get; }
        int NumberOfSeats { get; }
        void MoveTo(string city);
    }
}
