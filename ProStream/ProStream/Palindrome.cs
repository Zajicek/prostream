﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProStream
{
    public class Palindrome
    {
        public static bool IsAPalindrome(string word)
        {
            char[] wordInChars = word.ToCharArray();
            for (int i = 1; i <= (wordInChars.Length / 2); i++)
            {
                if (wordInChars[i - 1] != wordInChars[wordInChars.Length - i])
                {
                    return false;
                }
            }
            return true;
        }
    }
}
