﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProStream
{
    public class Vehicle : IVehicle
    {
        public string Location => _location;

        public int NumberOfSeats => 5;

        private string _location = string.Empty;

        public void MoveTo(string city)
        {
            if (String.IsNullOrWhiteSpace(city))
            {
                throw new NullReferenceException();
            }
            _location = city;
        }
    }
}
