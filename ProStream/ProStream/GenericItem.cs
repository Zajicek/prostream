﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProStream
{
    public class GenericItem<T>
    {
        public T Item => _item;

        private T _item;

        public void SetItem(T item)
        {
            _item = item;
        }
    }
}
