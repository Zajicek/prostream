using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ProStream.Test
{
    [TestClass]
    public class Test
    {
        [TestMethod]
        public void PalindromeTest()
        {
            Assert.IsTrue(!Palindrome.IsAPalindrome("abavba"));
            Assert.IsTrue(!Palindrome.IsAPalindrome("ab"));
            Assert.IsTrue(Palindrome.IsAPalindrome("abaaba"));
            Assert.IsTrue(Palindrome.IsAPalindrome("abah haba"));
            Assert.IsTrue(Palindrome.IsAPalindrome(""));
            Assert.IsTrue(Palindrome.IsAPalindrome("aa"));
        }

        [TestMethod]
        public void VehicleTest()
        {
            IVehicle vehicle = new Vehicle();
            vehicle.MoveTo("Prague");
            Assert.IsTrue(vehicle.Location == "Prague");
            vehicle.MoveTo("Brno");
            Assert.IsTrue(vehicle.Location == "Brno");
            Assert.IsTrue(vehicle.NumberOfSeats == 5);
        }

        [TestMethod]
        public void GenericItemTest()
        {
            GenericItem<string> genericItem = new GenericItem<string>();
            genericItem.SetItem("test");
            Assert.IsTrue(genericItem.Item == "test");
        }
    }
}
